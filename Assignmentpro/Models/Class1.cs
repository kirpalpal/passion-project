﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Assignmentpro.Models
{
    public class Class1
    {
        [Required(ErrorMessage="required")]
        public Nullable<int> auth_id { get; set; }
        [Required(ErrorMessage = "required")]
        public string bookname { get; set; }
        [Required(ErrorMessage = "required")]
        public string price { get; set; }
        [Required(ErrorMessage = "required")]
        public string booktype { get; set; }
    }
}