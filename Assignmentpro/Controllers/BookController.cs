﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Assignmentpro.Models;
namespace Assignmentpro.Controllers
{
    public class BookController : Controller
    {
        booksdbEntities3 db = new booksdbEntities3();
        //
        // GET: /Book/
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult AddAuthor()
        {
            return View();
        }
        [HttpPost]
        public ActionResult AddAuthor(authorname tb1)
        {
            db.authornames.Add(tb1);
            db.SaveChanges();
            ViewBag.mess = "author name inserted success";
            
            return View();

        }
        public ActionResult AddInfo()
        {
            List<authorname> at = db.authornames.ToList();
            @ViewBag.name = new SelectList(at, "id","authornam");
            return View();
        }
        [HttpPost]
        public ActionResult AddInfo(Class1 cl)
        {
            List<authorname> at = db.authornames.ToList();
            @ViewBag.name = new SelectList(at, "id", "authornam");
            authorinfo inf = new authorinfo();
            inf.auth_id = cl.auth_id;
            inf.bookname = cl.bookname;
            inf.price = cl.price;
            inf.booktype=cl.booktype;
            db.authorinfoes.Add(inf);
            db.SaveChanges();
            ViewBag.ins = "Books info inserted success";

            return View();
        }
        public ActionResult ShowInfo()
        {
            return View(db.authorinfoes.ToList());
        }
        [HttpPost]
        public ActionResult ShowInfo(int[] chkid)
        {
            foreach (int item in chkid)
            {
                var findid = db.authorinfoes.Where(x => x.auth_id == item).FirstOrDefault();
                var deleterec = db.authorinfoes.Remove(findid);
            }
            db.SaveChanges();
            return RedirectToAction("ShowInfo");
        }
        public ActionResult select(int id)
        {
            return View(db.authorinfoes.Where(x=>x.id==id).ToList());
        }
        [HttpPost]
        public ActionResult select(int id, string price)
        {
            var update = db.authorinfoes.Where(x=>x.id == id).FirstOrDefault();
            update.price = price;
            db.SaveChanges();
            return RedirectToAction("ShowInfo");
        }
       
       
	}
}