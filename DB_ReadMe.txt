create database booksdb

use booksdb

create table authorname(
id int identity primary key,
authornam varchar(20),
authorage int
)

create table authorinfo(
id int identity primary key,
auth_id int foreign key references authorname(id),
bookname varchar(20),
price varchar(20),
booktype varchar(20)
)